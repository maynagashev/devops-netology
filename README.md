# Домашние задания по курсу "DevOps"

1. [Введение в DevOps](01-intro)
2. Системы управления версиями (git)
    1. [Системы контроля версий](02-git-01-vcs) – настройка репозитория, .gitignore
    2. [Основы Git](02-git-02-base) – remotes, tags, branches, ide.